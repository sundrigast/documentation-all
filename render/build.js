/*
  Static HTML documents build script
 */

const krokiServer = 'https://kroki.io'
const fs = require('fs')
const asciidoctor = require('asciidoctor')()
const kroki = require('asciidoctor-kroki')
// default style attributes allowed to override in the document
const asciidoctorPreprocessor = require('./asciidoctor-preprocessor')
const minify = require('html-minifier').minify
const CleanCSS = require('clean-css')
const path = require('path')

function documentFilename(documentFile) {
    const pos = documentFile.lastIndexOf('/')
    return documentFile.substring(pos + 1, documentFile.lastIndexOf('.') - pos - 1)
}

module.exports = {

    /**
     * Build specified document
     * @param source document source file
     * @param destination filename
     * @param repositoryRootUri repository url for cross-links between documents
     */
    convert: function (source, destination, repositoryRootUri='.') {
        const documentFileName = source.includes('/') ? source.match(/\/([^/]+)\./)[1] : documentFilename(source)
        const filesPath = source.substring(0, source.lastIndexOf('/'))
        const dstDir = source.includes('/') ? source.substring(0, source.lastIndexOf('/')) + '/build/': 'build/'
        const destinationFile = destination ? dstDir + destination : dstDir + documentFileName + '.html'
        const cssSource = path.resolve(__dirname, 'default.css')
        const cssDestinationFilename = documentFileName + '.css'
        const cssDestinationPath = dstDir + 'images/' + cssDestinationFilename
        const sourcesPath = source.includes('/') ? filesPath + '/sources/' : 'sources/'

        if (!fs.existsSync(dstDir)) {
            fs.mkdirSync(dstDir, { recursive: true }, err => {
                if(err) throw err; // не удалось создать папки

                fs.mkdirSync(dstDir + '/images')
            });
        }

        if (!fs.existsSync(dstDir + '/images')) {
            fs.mkdirSync(dstDir + '/images')
        }

        if (fs.existsSync(sourcesPath)) {
            fs.cpSync(sourcesPath, dstDir + 'images/sources/', { recursive: true }, err => {
                if(err) throw err; // не удалось создать папки
                console.log('sources copied success!');
            });
        }

        console.log('Compiling CSS ' + cssSource + ' to ' + cssDestinationPath)
        const css = new CleanCSS({
            inline: ['local'],
            compatibility: '*',
            level: 0
        }).minify([cssSource]);

        if (css.errors && css.errors.length) {
            throw 'CSS compilation failed: ' + css.errors;
        }

        fs.writeFile(cssDestinationPath, css.styles, function (err) {
            if (err) return console.log(err);

            console.log('Converting ' + source + ' to ' + destinationFile)
            const registry = asciidoctor.Extensions.create()
            kroki.register(registry)
            asciidoctorPreprocessor.asciidoctorPreprocessor(registry)
            asciidoctorPreprocessor.inlineMacros(registry, false)

            base_html = asciidoctor.convertFile(source, {
                to_file: false,
                standalone: true,
                safe: 'safe',
                extension_registry: registry,
                attributes: {
                    'kroki-server-url': krokiServer,
                    'kroki-fetch-diagram': true,
                    'kroki-default-options': 'inline',
                    'imagesoutdir': dstDir + 'images',
                    'imagesdir': 'build/images',
                    'allow-uri-read': 'true',
                    'data-uri': 'true',
                    'icons': 'font',
                    'webfonts!': '',
                    'stylesheet': 'build/images/' + cssDestinationFilename,
                    'kroki-plantuml-include': path.resolve(__dirname, 'plantuml-styles.iuml'),
                    'uri-repository-root': repositoryRootUri
                }
            })

            const html = base_html.replace('<div id="content">', '<script src=""https://sundrigast.gitlab.io/documentation-all/styles/theme-switcher.js" type="text/javascript"></script><div id=content>')

            // check for errors
            const hasErrors = html.match("class=\"(.*)kroki-error(.*)\"")
            if(hasErrors != null) {
                throw new Error('Build failed. Kroki returns an error while building diagram.');
            }

            const minifiedHtml = minify(html, {
                removeAttributeQuotes: true,
                removeComments: true,
                minifyCSS: true,
                minifyJS: true,
                collapseWhitespace: true
            });

            fs.writeFileSync(destinationFile, minifiedHtml, function (err) {
                if (err) return console.log(err);
            })

            if (dstDir !== 'build/') {
                fs.cpSync(dstDir, 'build/' + filesPath, { recursive: true }, err => {
                    if(err) throw err;
                    console.log('sources copied success!');
                });

                fs.rm(dstDir, {recursive:true}, (err) => {
                    if(err) throw err;
                    }
                );
            }
        })
    }
}