const renderer = require('./build.js')
const exec = require('node:child_process')

exec.exec("find . -name '*.adoc' -not -path './node_modules/*' ", (error, stdout) => {
    if (error) {
        console.error(`exec error: ${error}`);
        return;
    }

    let docsList = stdout.substring(2).slice(0, -1).split('\n' + './');

    docsList.forEach(function(doc) {
        renderer.convert(doc);
    });
});