purgeCss();
addSwitcher();

function switchCss(css) {
  link = document.getElementsByTagName("link");
  last = link[link.length - 1];

  if(css){
    last.href = "https://sundrigast.gitlab.io/documentation-all/styles/css/dark.css";
  } else {

    last.href = "https://sundrigast.gitlab.io/documentation-all/styles/css/light.css";
  }
}

function capitalize(s) {
  u = s.replace(/^(.)/, function(_, l){
    return l.toUpperCase();
  });
  return u;
}


function addSwitcher() {
  head = document.getElementsByTagName('head')[0];
  link = document.createElement('link');
  link.rel="stylesheet";
  link.type="text/css";
  link.href="https://sundrigast.gitlab.io/documentation-all/styles/css/light.css";
  head.appendChild(link);

  var header = document.getElementById("toc");
  header.insertAdjacentHTML("beforeend", '        <div class="switcher">\n' +
      '            <label class="switch">\n' +
      '                <input type="checkbox" onchange="switchCss(this.checked)">\n' +
      '                <span class="slider"></span>\n' +
      '            </label>\n' +
      '        </div>');

}


function removeDefault() {
  var styles = document.getElementsByTagName("style");
  for (var i = 0; i < styles.length; i++) {
    if (/\* Asciidoctor default stylesheet/.test(styles[i].textContent)) {
      removeNode(styles[i]);
    }
  }
}

function removeClean() {
  var links = document.getElementsByTagName("link");
  for (var i = 0; i < links.length; i++) {
    if (/clean\.css/.test(links[i].href)) {
      removeNode(links[i]);
    }
  }
}

function isEmpty(obj) {
  for(var prop in obj) {
    if(obj.hasOwnProperty(prop)) {
      return false;
    }
  }
  return true;
}

function removeNode(n) {
  n.parentNode.removeChild(n);
}

function purgeCss() {
  removeDefault();
  removeClean();
}