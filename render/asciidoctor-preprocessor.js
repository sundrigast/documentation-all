/*
Asciidoctor document preprocessor defines default set of style attributes.
These attributes allowed to override in end user document.
*/
function asciidoctorPreprocessor(registry) {
    registry.preprocessor(function () {
        const self = this
        self.process(function (doc, reader) {
            doc.removeAttribute('figure-caption')
            doc.setAttribute('toc', 'left')
            doc.setAttribute('toc-title', 'Contenido')
            doc.setAttribute('toclevels', '3')
            doc.setAttribute('sectnums', '')
            doc.setAttribute('sectanchors', '')
            doc.setAttribute('table-caption', 'Table')
            return reader
        })
    })
}

function inlineMacros(registry, online = true) {
    registry.inlineMacro('form', function () {
        var self = this
        self.process(function (parent, target) {
            return ''
        })
    })
}

if (typeof module !== 'undefined') {
    module.exports = {
        asciidoctorPreprocessor: asciidoctorPreprocessor,
        inlineMacros: inlineMacros
    }
}